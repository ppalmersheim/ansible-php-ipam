# Ansible PHP-IPAM

This will install and configure php-ipam.

This will use MariaDB and NGINX.

## How to use

Create a copy of the inventory file

```
cp inventory.example.yml inventory.yml
```

Modify the host name, and the first 3 variables to your needs. 


## notes
This will need to be tested with php8.0

## Current issues

- mysql install is odd. It will create the root login, but with socket based sign in not password
- need to add tasks to setup the automatic scanning functions at least the cli portion
- 